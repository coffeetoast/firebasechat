//
//  Extension.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 27..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

extension UIView
{
    func anchor(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil) {
        
        anchor(top: top, left: left, bottom: bottom, right: right, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
    
    func anchor(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0) {
        
        _ = anchor(top: top, topConstant: topConstant, left: left, leftConstant: leftConstant, bottom: bottom, bottomConstant: bottomConstant, right: right, rightConstant: rightConstant, widthConstant: 0, heightConstant: 0)
    }
    
    func anchor(top: NSLayoutYAxisAnchor? = nil, topConstant: CGFloat = 0, left: NSLayoutXAxisAnchor? = nil, leftConstant: CGFloat = 0, bottom: NSLayoutYAxisAnchor? = nil, bottomConstant: CGFloat = 0, right: NSLayoutXAxisAnchor? = nil, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({ $0.isActive = true })
        
        return anchors
        
    }
    
}

let imageCahe = NSCache<NSString, AnyObject>()

extension UIImageView
{
    fileprivate struct StringKeys {
        static var imageUrl = "cached_image"
    }
    
    var imageUrlString: String? {
        get {
            return objc_getAssociatedObject(self, &StringKeys.imageUrl) as? String
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &StringKeys.imageUrl, newValue as String?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    func loadImageUsingCache(urlString: String , completionHandler: ((_ success: Bool)->())? ) {
        imageUrlString = urlString
        
        let url = NSURL(string: urlString)
        
        image = nil
        
        if let imageFromCache = imageCahe.object(forKey: urlString as NSString) as? UIImage {
            self.image = imageFromCache

            if let handler = completionHandler {
                return handler(true)
            }
            return
        }
        
        URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            
            if error != nil {
                print(error!)
                if let hander = completionHandler {
                    return hander(false)
                }
                return
            }
            
            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)
                
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                
                imageCahe.setObject(imageToCache!, forKey: urlString as NSString)
                
                if let hander = completionHandler {
                    return hander(true)
                }
                return
            }
            
            }.resume()
        
    }
}

extension UIImage
{
    func resizeImage(newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

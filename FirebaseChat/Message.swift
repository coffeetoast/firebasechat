//
//  Message.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 28..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {
    var fromId: String?
    var text: String?
    
    var imageUrl: String?
    var imageWidth: NSNumber?
    var imageHeight: NSNumber?
    
    var timestamp: NSNumber?
    var toId: String?

    func chatPartnerId() -> String? {
        return fromId == FIRAuth.auth()?.currentUser?.uid ? toId : fromId
    }
}

//
//  MessageCellTableViewCell.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 29..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import Firebase

class ChatMessageCell: UICollectionViewCell
{
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = "sample Text For Now..."
        tv.backgroundColor = .clear
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.textColor = .white
        tv.isEditable = false
        return tv
    }()
    
    let bubbleView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor(r: 0, g: 137, b: 249)
        view.layer.cornerRadius = 18
        view.layer.masksToBounds = true 
        return view
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "car")
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let messageImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        //imageView.backgroundColor = .brown
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(bubbleView)
        addSubview(textView)
        addSubview(profileImageView)
        
        bubbleView.addSubview(messageImageView)
        setupMessageImageView()
        
        
        setupBubbleView()
        setupTextView()
        setupProfileImageView()
    }
    
    func setupMessageImageView() {
        _ = messageImageView.anchor(top: bubbleView.topAnchor, topConstant: 0, left: bubbleView.leftAnchor, leftConstant: 0, bottom: bubbleView.bottomAnchor, bottomConstant: 0, right: bubbleView.rightAnchor, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func setupProfileImageView() {
        _ = profileImageView.anchor(top: nil, topConstant: 0, left: leftAnchor, leftConstant: 8, bottom: bottomAnchor, bottomConstant: 0, right: nil, rightConstant: 0, widthConstant: 32, heightConstant: 32)
    }
    
    
    var bubbleConstraints: [NSLayoutConstraint]?
    
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleRightAnchor: NSLayoutConstraint?
    var bubbleLeftAnchor: NSLayoutConstraint?
    
    func setupBubbleView() {
        bubbleConstraints = bubbleView.anchor(top: topAnchor, topConstant: 0, left: profileImageView.rightAnchor, leftConstant: 8, bottom: nil, bottomConstant: 0, right: rightAnchor, rightConstant: 8, widthConstant: 200, heightConstant: 0)
        bubbleWidthAnchor = bubbleConstraints?[3]
        bubbleRightAnchor = bubbleConstraints?[2]
        bubbleLeftAnchor = bubbleConstraints?[1]
        bubbleLeftAnchor?.isActive = false
        bubbleView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
    }
    
    
    func setupTextView() {
       _ = textView.anchor(top: topAnchor, topConstant: 0, left: bubbleView.leftAnchor, leftConstant: 8, bottom: nil, bottomConstant: 0, right: bubbleView.rightAnchor, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        textView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

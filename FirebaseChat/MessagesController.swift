//
//  ViewController.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 25..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import Firebase
class MessagesController: UITableViewController {

    private let cellId = "CellId"
    
    //Model......................................
    var messages = [Message]()
    var lastmessagePerUserDic = [String:Message]()
    
    
    // ViewLife Cycle............................
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New", style: .plain, target: self, action: #selector(handleNewMessage))
        
        // user is not logged in 
        checkIfUserIsLoggedIn()
    }
    
    // checkLogin... fetchUser....SetupNavBar....observeMyMessage........
    func checkIfUserIsLoggedIn() {
        if FIRAuth.auth()?.currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
            fetchUserAndSetupNavBarTitle()
        }
    }
    
    func fetchUserAndSetupNavBarTitle() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else { return }
        
        FIRDatabase.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            print(snapshot)
            if let dic = snapshot.value as? [String:AnyObject] {
                //self.navigationItem.title = dic["name"] as? String
                
                let user = User()
                user.setValuesForKeys(dic)
                self.setupNavBarWithUser(user: user)
            }
            
        }, withCancel: nil)
    }
    
    func setupNavBarWithUser(user: User) {
        messages.removeAll()
        lastmessagePerUserDic.removeAll()
        tableView.reloadData()
        
        observeMyMessages()
        
        self.navigationItem.title = user.name
        
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        //titleView.backgroundColor = .red
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        titleView.addSubview(containerView)
        
        let profileImageView = UIImageView()
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        if let profileImageUrl = user.profileImageUrl {
            profileImageView.loadImageUsingCache(urlString: profileImageUrl, completionHandler: nil)
        }
        
        containerView.addSubview(profileImageView)
        _ = profileImageView.anchor(top: nil, topConstant: 0, left: containerView.leftAnchor, leftConstant: 0, bottom: nil, bottomConstant: 0, right: nil, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        profileImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
        let nameLabel = UILabel()
        nameLabel.text = user.name
        
        containerView.addSubview(nameLabel)
        
        _ = nameLabel.anchor(top: nil, topConstant: 0, left: profileImageView.rightAnchor, leftConstant: 8, bottom: nil, bottomConstant: 0, right: containerView.rightAnchor, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        nameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor).isActive = true
        
        containerView.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        
        self.navigationItem.titleView = titleView
        
        //titleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showChatControllerForUser)))
    }
    
    func observeMyMessages() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else { return }
        
        let ref = FIRDatabase.database().reference().child("my-messages").child(uid)
        
        ref.observe(.childAdded, with: { (snapshot) in
            let userId = snapshot.key
            ref.child(userId).queryLimited(toLast: 1).observe(.childAdded, with: { (snapshot) in
                let messageId = snapshot.key
                let messageReference = FIRDatabase.database().reference().child("messages").child("\(messageId)")
                
                messageReference.observeSingleEvent(of: .value, with: { (snapshot) in
                    if let dic = snapshot.value as? [String: AnyObject] {
                        let message = Message()
                        message.setValuesForKeys(dic)
                        //self.messages.append(message)
                        
                        if let chatPartnerId = message.chatPartnerId() {
                            self.lastmessagePerUserDic[chatPartnerId] = message
                            
                            self.messages = Array(self.lastmessagePerUserDic.values)
                        }
                        
                        self.timer?.invalidate()
                        
                        self.timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
                    }
                    
                })
            }, withCancel: nil)
        })
        
    }
    
    var timer: Timer?
    
    func handleReloadTable() {
        print("reload tableview!!!!")
        self.tableView.reloadData()
    }


    //TableViewDataSource................................
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cellId")
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let message = messages[indexPath.row]
        cell.message = message
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        let ref = FIRDatabase.database().reference().child("users").child(chatPartnerId)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dic = snapshot.value as? [String:AnyObject] else { return }
            
            let user = User()
            user.id = chatPartnerId
            user.setValuesForKeys(dic)
            self.showChatControllerForUser(user)
            
        })
    }
    
    // Push chatLogController When didSelect............................
    func showChatControllerForUser(_ user: User) {
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        
        chatLogController.user = user
        navigationController?.pushViewController(chatLogController, animated: true)
    }
    
    
    //Handle New Button..................................
    func handleNewMessage() {
        let newMessageController = NewMessageController()
        newMessageController.messagesController = self
        let navController = UINavigationController(rootViewController: newMessageController)
        present(navController, animated: true, completion: nil)
    }
    

    
    //Handle Logout Button.............................
    func handleLogout() {
        
        do {
            try FIRAuth.auth()?.signOut()
        } catch let logoutError {
            print(logoutError)
        }
        let loginController = LoginController()
        loginController.messagesController = self
        present(loginController, animated: true, completion: nil)
        
    }
}


//
//  UserCell.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 28..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import Firebase

class UserCell: UITableViewCell
{
    
    var message: Message? {
        didSet {
            setupNameAndProfileImage()
            
            detailTextLabel?.text = message?.text
            
            if let seconds = message?.timestamp?.doubleValue {
                let timestampDate = Date(timeIntervalSince1970: seconds)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm:ss a"
                timeLabel.text = dateFormatter.string(from: timestampDate)
                
            }
        }
    }
    
    private func setupNameAndProfileImage() {
        if let id = message?.chatPartnerId() {
            let ref = FIRDatabase.database().reference().child("users").child(id)
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dic = snapshot.value as? [String:AnyObject] {
                    self.textLabel?.text = dic["name"] as? String
                    self.profileImageView.loadImageUsingCache(urlString: dic["profileImageUrl"] as! String, completionHandler: nil)
                }
            }, withCancel: nil)
        }
    }

    var user : User? {
        didSet {
            if let user = user {
                textLabel?.text = user.name
                detailTextLabel?.text = user.email
                
                if let profileImageUrl = user.profileImageUrl {
                    
                    self.profileImageView.loadImageUsingCache(urlString: profileImageUrl, completionHandler:nil)
                    
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = CGRect(x: 64, y: (textLabel!.frame.origin.y - 2), width: (textLabel!.frame.size.width), height: (textLabel!.frame.size.height))
        
        detailTextLabel?.frame = CGRect(x: 64, y: (detailTextLabel!.frame.origin.y + 2), width: (self.frame.size.width - 86), height: (detailTextLabel!.frame.size.height))
        
    }
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        //imageView.image = UIImage(named: "car")
        imageView.layer.cornerRadius = 24
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let timeLabel: UILabel = {
       let label = UILabel()
       // label.text = "HH:MM:SS"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .lightGray
        return label
    }()
    
   
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    func setupViews() {
        addSubview(profileImageView)
        addSubview(timeLabel)
        
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        _ = profileImageView.anchor(top: nil, topConstant: 0, left: leftAnchor, leftConstant: 8, bottom: nil, bottomConstant: 0, right: nil, rightConstant: 0, widthConstant: 48, heightConstant: 48)
        
        // x,y,w,h 
        _ = timeLabel.anchor(top: nil, topConstant: 0, left: nil, leftConstant: 0, bottom: nil, bottomConstant: 0, right: rightAnchor, rightConstant: 0, widthConstant: 100, heightConstant: 0)
        timeLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -10).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: (textLabel?.heightAnchor)!).isActive = true
        
      
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

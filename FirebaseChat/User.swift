//
//  User.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 27..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

class User: NSObject
{
    var id: String?
    var name: String?
    var email: String?
    var profileImageUrl: String?
}

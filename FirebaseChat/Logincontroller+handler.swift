//
//  Logincontroller+handler.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 27..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import Firebase

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func handleRegister() {
        guard let email = emailTextField.text, let password = passwordTextField.text, let name = nameTextField.text else {
            print("Form is not valid")
            return
        }
        
            FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, err) in
                guard err == nil else { return }
                guard let uid = user?.uid else { return }
                
                if Thread.isMainThread {
                    print("is Main!!!!!!!!!!!!!!!!")
                }
                
                let imageName = NSUUID().uuidString

                BlockOperation(block: {
            
                    let storageRef = FIRStorage.storage().reference().child("profile_image").child("\(imageName).jpg")
                    let metaData = FIRStorageMetadata()
                    metaData.contentType = "image/jpg"
                    
                    if let uploadData = UIImageJPEGRepresentation(self.profileImageView.image!, 0.8) {
                        let uploadTask = storageRef.put(uploadData, metadata: metaData, completion: { (metadata, error) in
                            guard error == nil else { return }
                            
                            if let profileImageUrl = metadata?.downloadURL()?.absoluteString {
                                let values = ["name": name as AnyObject, "email": email as AnyObject, "profileImageUrl": profileImageUrl as AnyObject]
                                self.registerUserIntoDatabaseWithUID(uid: uid, values: values)
                            }
                        })
                        
                        uploadTask.observe(.progress, handler: { (snapshot) in
                            // Upload reported progress
                            if let progress = snapshot.progress {
                                _ = 100.0 * Double(progress.completedUnitCount) / Double(progress.totalUnitCount)
                                print("upload progress:",progress)
                            }
                        })
                        
                        uploadTask.observe(.success) { snapshot in
                            // Upload completed successfully
                            print("upload image Succes..................")
                        }
                    }
                }).start()
                
            })
        
    }

    fileprivate func registerUserIntoDatabaseWithUID(uid: String, values: [String:AnyObject]) {
        // successfully authenticated user........
        let ref = FIRDatabase.database().reference()
        let userReference = ref.child("users").child(uid)
        userReference.updateChildValues(values, withCompletionBlock: { (error, ref) in
            guard error == nil else { return }
            
            //saved successfully into Firebase db.....
            let user = User()
            user.setValuesForKeys(values)
            self.messagesController?.setupNavBarWithUser(user: user)
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    
    // UIImagePickerControllerDelegate .....................
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("canceld picker")
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileImageView.image = selectedImage
        }
        
        
        dismiss(animated: true, completion: nil)
    }
}

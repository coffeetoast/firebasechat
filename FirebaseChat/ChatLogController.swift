//
//  ChatLogController.swift
//  FirebaseChat
//
//  Created by SungJaeLEE on 2016. 11. 28..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import Firebase

class ChatLogController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    fileprivate let cellId = "cellId"
    
    // Model ......................
    var messages = [Message]()
    var user: User? {
        didSet {
            navigationItem.title = user?.name
            observeMessages()
        }
    }
    
    func observeMessages() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid, let toId = user?.id else { return }
        
        let userMessagesRef = FIRDatabase.database().reference().child("my-messages").child(uid).child(toId)
        userMessagesRef.observe(.childAdded, with: { (snapshot) in
            let messageId = snapshot.key
            let messageRef = FIRDatabase.database().reference().child("messages").child(messageId)
            messageRef.observeSingleEvent(of: .value, with: { (snapshot) in
                guard let dic = snapshot.value as? [String:AnyObject] else {
                    return
                }
                
                let message = Message()
                message.setValuesForKeys(dic)
                self.messages.append(message)
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    // scroll to last index
                    let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
                }
               
            })
        })
        
    }
    
    
    // SubViews.............................................
    lazy var inputTextField: UITextField = {
        let inputTextField = UITextField()
        inputTextField.placeholder = "Enter message..."
        inputTextField.delegate = self
        return inputTextField
    }()
    
    lazy var inputContainerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
        
        let uploadLabel = UILabel()
        uploadLabel.text = "+"
        uploadLabel.textAlignment = .center
        uploadLabel.textColor = .lightGray
        uploadLabel.font = UIFont.boldSystemFont(ofSize: 20)
        uploadLabel.isUserInteractionEnabled = true
        uploadLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUploadTap)))
        containerView.addSubview(uploadLabel)
        
        //x,y,w,h 
        _ = uploadLabel.anchor(top: nil, topConstant: 0, left: containerView.leftAnchor, leftConstant: 0, bottom: nil, bottomConstant: 0, right: nil, rightConstant: 0, widthConstant: 44, heightConstant: 44)
        uploadLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
        let sendButton = UIButton(type: .system)
        sendButton.setTitle("Send", for: .normal)
        sendButton.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        
        containerView.addSubview(sendButton)
        
        //x,y,w,h
        _ = sendButton.anchor(top: nil, topConstant: 0, left: nil, leftConstant: 0, bottom: nil, bottomConstant: 0, right: containerView.rightAnchor, rightConstant: 0, widthConstant: 80, heightConstant: 0)
        sendButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        sendButton.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        
        containerView.addSubview(self.inputTextField)
        
        // x,y,w,h
        _ = self.inputTextField.anchor(top: nil, topConstant: 0, left: uploadLabel.rightAnchor, leftConstant: 0, bottom: nil, bottomConstant: 0, right:sendButton.leftAnchor, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.inputTextField.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        self.inputTextField.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        let seperatorLineView = UIView()
        seperatorLineView.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        
        containerView.addSubview(seperatorLineView)
        // x,y,w,h
        _ = seperatorLineView.anchor(top: containerView.topAnchor, topConstant: 0, left: containerView.leftAnchor, leftConstant: 0, bottom: nil, bottomConstant: 0, right: containerView.rightAnchor, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        
        return containerView
    }()
    
    
    // HandleUploadTap.....................................
    func handleUploadTap() {
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
             uploadToFirebaseStorageUsingImage(image: selectedImage)
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func uploadToFirebaseStorageUsingImage(image: UIImage) {
        let imageName = NSUUID().uuidString

        let ref = FIRStorage.storage().reference().child("message_images").child(imageName)
        let metaData = FIRStorageMetadata() ;metaData.contentType = "image/jpg"
        
        if let uploadData = UIImageJPEGRepresentation(image, 0.5) {
            let uploadTask = ref.put(uploadData, metadata: metaData, completion: { (metaData, error) in
                guard error == nil else { print("Failed upoload") ;return }
                
                if let imageUrl = metaData?.downloadURL()?.absoluteString {
                    self.sendMessageWithImageUrl(imageUrl: imageUrl, image: image)
                }
            })
            
            uploadTask.observe(.progress, handler: { (snapshot) in
                // Upload reported progress
                if let progress = snapshot.progress {
                    _ = 100.0 * Double(progress.completedUnitCount) / Double(progress.totalUnitCount)
                    print("upload progress:",progress)
                }
            })
            
            uploadTask.observe(.success) { snapshot in
                // Upload completed successfully
                print("upload image Succes..................")
            }

        }
    }
    

    
    // Handling Send Button........................
    func handleSend() {
        let properies = ["text": inputTextField.text!]
        sendMessageWithProperties(properties: properies as [String : AnyObject])
        
        inputTextField.text = nil
    }

    // Actual Send Message with ImageUrl................
    fileprivate func sendMessageWithImageUrl(imageUrl: String, image: UIImage) {
        
        let properties = ["imageUrl": imageUrl ,"imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
        sendMessageWithProperties(properties: properties as [String : AnyObject])
    }
    
    fileprivate func sendMessageWithProperties(properties: [String:AnyObject]) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        
        let toId = user!.id!
        let fromId = FIRAuth.auth()!.currentUser!.uid
        let timestamp: NSNumber = Int(NSDate().timeIntervalSince1970) as NSNumber
        var values = ["toId":toId , "fromId": fromId , "timestamp": timestamp] as [String : Any]
        
        properties.forEach({ values[$0] = $1 })
        
        childRef.updateChildValues(values) { (error,ref) in
            guard error == nil else { print(error!); return }
            
            // FanOut...........................
            let myMessageRef = FIRDatabase.database().reference().child("my-messages").child(fromId).child(toId)
            
            let messageId = childRef.key
            myMessageRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = FIRDatabase.database().reference().child("my-messages").child(toId).child(fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
    }

    
    
    // Handling Keyboard .....................
    override var inputAccessoryView: UIView? {
        return inputContainerView
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    // ViewLife Cycle..............................
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationItem.title = "Chat Log Controller"
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        //collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = .white
        collectionView?.keyboardDismissMode = .interactive
        
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: cellId)
        
        setupKeyboardObservers()
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: Notification.Name.UIKeyboardDidShow, object: nil)
    }
    
    func handleKeyboardDidShow() {
        if messages.count > 0 {
            let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
}

extension ChatLogController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        handleSend()
        textField.text = nil
        return true
    }
}

extension ChatLogController: UICollectionViewDelegateFlowLayout
{
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatMessageCell
        
        let message = messages[indexPath.item]
        cell.textView.text = message.text
        
        setupCell(cell: cell, message: message)
        
        if let text = message.text {
            cell.bubbleWidthAnchor?.constant = estimateFrameForText(text: text).size.width + 30
        } else if message.imageUrl != nil {
            // fall in .............
            cell.bubbleWidthAnchor?.constant = 200
        }
        //cell.backgroundColor = .blue
        return cell
    }
    
    fileprivate func setupCell(cell:ChatMessageCell, message: Message) {
        if let profileImageUrl = self.user?.profileImageUrl {
            cell.profileImageView.loadImageUsingCache(urlString: profileImageUrl, completionHandler: nil)
        }
        

        
        if message.fromId == FIRAuth.auth()?.currentUser?.uid {
            cell.bubbleView.backgroundColor = UIColor(r: 0, g: 137, b: 249)
            cell.textView.textColor = .white
            cell.profileImageView.isHidden = true
            
            cell.bubbleRightAnchor?.isActive = true
            cell.bubbleLeftAnchor?.isActive = false
            
        } else {
            cell.bubbleView.backgroundColor = UIColor(r: 240, g: 240, b: 240)
            cell.textView.textColor = .black
            
            cell.profileImageView.isHidden = false
            cell.bubbleRightAnchor?.isActive = false
            cell.bubbleLeftAnchor?.isActive = true
        }

        
        if let messageImageUrl = message.imageUrl {
            cell.bubbleView.backgroundColor = .clear
            
            cell.messageImageView.loadImageUsingCache(urlString: messageImageUrl, completionHandler: nil)
            cell.messageImageView.isHidden = false
        } else {
            cell.messageImageView.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]
        if let text = message.text {
            height = estimateFrameForText(text: text).size.height + 20
        } else if let imageWidth = message.imageWidth?.floatValue, let imageHeight = message.imageHeight?.floatValue {
            
            // h1 : w1 = h2 / w2 
            // h1 = h2 / w2 * w1
            
            height = CGFloat(imageHeight / imageWidth * 200)
        }
        
        
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }
    
    fileprivate func estimateFrameForText(text: String) -> CGRect {
        return NSString(string: text).boundingRect(with: CGSize(width:200, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin), attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16)], context: nil)
    }

    
    
}
